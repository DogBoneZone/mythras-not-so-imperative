Hooks.on('renderChatMessage', (app, html, data) => {
    const messageDoc = game.messages.find(i => i.id == html[0].dataset.messageId)
    let chatButtons = [...html[0].querySelectorAll('.submit-damage')]
    if (chatButtons.length == 0) return
    let revealButton = html[0].querySelector(".revealDamage")
    let damageElement = html[0].querySelector(".damageElement")
    let targetTokenActor = game.scenes.current.tokens.find(token => token.id == chatButtons[0].dataset.targetToken)

    revealButton.addEventListener("click", function revealDamage() {
      damageElement.classList.toggle('revealed')
    })

    function applyDamage(damageButton, hitLocationId, armorPoints, weaponDamage, damageFormula) {
          if (game.user.isGM) {
            let hitLocation = targetTokenActor.actor.getEmbeddedDocument('Item', hitLocationId)
            let armorMitigatedDamage = Number(weaponDamage) > (armorPoints) ? Number(weaponDamage - (armorPoints)) : 0
            let updatedHp = Number(hitLocation.data.data.currentHp - armorMitigatedDamage)
            hitLocation.update({'data.currentHp': updatedHp})
            damageButton.classList.toggle('damage-applied')
            messageDoc.setFlag('mythras-not-so-imperative', 'damage-applied', true)
          }
    }
  
    for (let damageButton of chatButtons) {
        if (damageButton) {
            let isClicked = messageDoc.getFlag('mythras-not-so-imperative', 'damage-applied')
            if (isClicked) damageButton.classList.toggle('damage-applied')
            
            // Initialize variables to pass through Apply Damage Function
            let hitLocationId = damageButton.dataset.hitLocationId
            let armorPoints = Number(damageButton.dataset.armor)
            let weaponDamage = Number(damageButton.dataset.damage)
            let damageFormula = damageButton.dataset.damageFormula

            switch (damageButton.classList[0]) {
                case 'simple-damage': 
                    damageButton.textContent = 'Apply Damage'
                    if (!isClicked) {
                      damageButton.addEventListener("click", function() {applyDamage(damageButton, hitLocationId, armorPoints, weaponDamage, damageFormula)}, {once: true})
                    }
                    break

                case 'bypass-armor':
                    damageButton.textContent = 'Bypass Armor'
                    if (!isClicked) {
                      damageButton.addEventListener("click", function bypassArmorSelect() {
                          let d = new Dialog({
                            title: "Select Armor to Bypass",
                            content: `<div>
                                          Select which armor to bypass:
                                      </div>`,
                            buttons: {
                              one: {
                                label: "Natural",
                                callback: html => {
                                  applyDamage(damageButton, hitLocationId, 0, armorPoints, weaponDamage, damageFormula)
                                }
                              },
                              two: {
                                label: "Worn",
                                callback: html => {
                                  applyDamage(damageButton, hitLocationId, naturalArmor, 0, weaponDamage, damageFormula)
                                }
                              },
                              three: {
                                label: "Cancel",
                                callback: html => console.log('Cancelled')
                              }
                            },
                            default: "three",
                            close: html => console.log()
                          })

                          d.render(true)
                      }, {once: true})
                    }
                    
                    break
                    
                case 'choose-location': 
                    damageButton.textContent = 'Choose Location'
                    if (!isClicked) {
                      damageButton.addEventListener("click", function chooseLocation() {
                          let hitLocations = targetTokenActor.actor.items.filter(item => item.type == 'hitLocation')
                          hitLocations.sort((location1, location2) => {
                            let rangeStart1 = location1.data.data.rollRangeStart
                            let rangeStart2 = location2.data.data.rollRangeStart

                            if (rangeStart1 >= rangeStart2) return -1
                            if (rangeStart1 < rangeStart2) return 1
                        })

                          let hitLocationSelections = []
                          for (let hitLocation of hitLocations) {
                            hitLocationSelections.push(`<tr>
                                                        <td>${hitLocation.data.data.rollRangeStart}-${hitLocation.data.data.rollRangeEnd}</td>
                                                        <td>${hitLocation.name}</td>
                                                        <td><input type="checkbox" id="${hitLocation.id}"></td>
                                                        </tr>`)
                          }

                          let d = new Dialog({
                            title: "Choose new Hit Location",
                            content: `<div>
                                          Select a new Hit Location to strike:

                                          <table>
                                              <thead>
                                                  <tr>
                                                      <th>Range</th>
                                                      <th>Location</th>
                                                      <th>Select</th>
                                                  </tr>
                                              </thead>
                                              <tbody style="text-align: center;">
                                                  ${hitLocationSelections.join('')}
                                              </tbody>
                                          </table>
                                      </div>`,
                            buttons: {
                              one: {
                                label: "Apply Damage",
                                callback: html => {
                                    let newHitLocation = [...html[0].querySelectorAll("input[type='checkbox']")].find(selection => selection.checked).id
                                    let newHitLocationItem = targetTokenActor.actor.getEmbeddedDocument('Item', newHitLocation)
                                    let newArmorPoints = Number(newHitLocationItem.data.data.ap)
                                    applyDamage(damageButton, newHitLocation, newArmorPoints, weaponDamage, damageFormula)
                                }
                              },
                              two: {
                                label: "Cancel",
                                callback: html => console.log("Cancelled")
                              }
                            },
                            default: "two",
                            close: html => console.log()
                          })

                          d.render(true)
                      }, {once: true})
                    }

                    break

                case 'impale':
                    damageButton.textContent = 'Impale Damage'
                    let secondRoll = new Roll(damageButton.dataset.damageFormula)
                    secondRoll.roll({async: false})

                    if (!isClicked) {
                      damageButton.addEventListener("click", function() {applyDamage(damageButton, hitLocationId, armorPoints, Math.max(weaponDamage, Number(secondRoll.total)), damageFormula)}, {once: true})
                      damageButton.addEventListener("click", function showSecondRoll() {
                        ChatMessage.create({
                          roll: secondRoll,
                          type: CONST.CHAT_MESSAGE_TYPES.ROLL
                        })
                    }, {once: true})
                  }
                    break
            }

            if (!game.user.isGM) damageButton.style.display = 'none'
      
          }
        }
  })